class CreateLivestockTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :livestock_types do |t|
      t.string :name
      t.integer :super_type_id

      t.timestamps
    end
  end
end
