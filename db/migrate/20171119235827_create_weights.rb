class CreateWeights < ActiveRecord::Migration[5.1]
  def change
    create_table :weights do |t|
      t.decimal :kilos
      t.datetime :date

      t.timestamps
    end
  end
end
