class CreateLivestock < ActiveRecord::Migration[5.1]
  def change
    create_table :livestock do |t|
      t.string :name
      t.datetime :birthday
      t.datetime :deceased
      t.string :note

      t.integer :gender_id
      t.integer :primary_color_id
      t.integer :secondary_color_id
      t.integer :primary_photo_id
      t.integer :bought_id
      t.integer :sold_id
      t.integer :tag_id
      t.integer :father_id
      t.integer :mother_id
      t.integer :type_id

      t.timestamps
    end
  end
end
