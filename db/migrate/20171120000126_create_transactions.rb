class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.decimal :total
      t.integer :amount
      t.datetime :date
      t.string :note
      t.integer :weight_id

      t.timestamps
    end
  end
end
