class CreateLivestockWeights < ActiveRecord::Migration[5.1]
  def change
    create_table :livestock_weights do |t|
      t.integer :weight_id
      t.integer :livestock_id
    end
  end
end
