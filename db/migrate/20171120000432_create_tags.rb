class CreateTags < ActiveRecord::Migration[5.1]
  def change
    create_table :tags do |t|
      t.integer :color_id
      t.string :number
      t.string :note

      t.timestamps
    end
  end
end
