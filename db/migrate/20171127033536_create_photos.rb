class CreatePhotos < ActiveRecord::Migration[5.1]
  def change
    create_table :photos do |t|
      t.datetime :date
      t.string :file_name
      t.string :file_path
      t.string :checksum
      t.timestamps
    end
     create_join_table :livestock, :photos do |t|
      t.integer :photo_id
      t.integer :livestock_id
      t.timestamps
    end
  end
end
