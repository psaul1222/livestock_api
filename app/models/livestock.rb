class Livestock < ApplicationRecord
  belongs_to :tag
  belongs_to :type, class_name:'LivestockType', foreign_key:'type_id'
  belongs_to :primary_color, class_name:'Color', foreign_key:'primary_color_id'
  belongs_to :secondary_color, class_name:'Color', foreign_key:'secondary_color_id'
  belongs_to :gender
  belongs_to :primary_photo, class_name:'Photo', foreign_key:'primary_photo_id'
  belongs_to :bought, class_name:'Transaction', foreign_key:'bought_id'
  belongs_to :sold, class_name:'Transaction', foreign_key:'sold_id'
  belongs_to :father, class_name:'Livestock', foreign_key:'father_id'
  belongs_to :mother, class_name:'Livestock', foreign_key:'mother_id'
  has_and_belongs_to_many :photos
  validates_presence_of :gender,
      :message => Proc.new { |livestock, data|
      "You must provide #{data[:attribute]} for #{livestock.gender}"
      }
end
