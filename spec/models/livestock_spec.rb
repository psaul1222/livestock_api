require 'rails_helper'

describe Livestock, type: :model do
  it "is has valid factory" do
    expect(FactoryBot::create(:livestock,:father,:mother)).to be_valid
  end
  it "is invalid without birthday" do
    expect(FactoryBot::build(:livestock,:father,:mother,birthday:nil)).to be_valid
  end
end
