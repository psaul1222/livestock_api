require 'faker'

FactoryBot.define do
  factory :transaction do |f|
    f.amount { Faker::Number.number(3) }
    f.date { Faker::Date.backward(1)}
    f.total { Faker::Number.decimal(3,2) }
    f.note { Faker::Name.name }
  end
end
