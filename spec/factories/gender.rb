require 'faker'

FactoryBot.define do
  factory :gender do |f|
    f.type { Faker::Name.name }
  end
end
