require 'faker'

FactoryBot.define do
  factory :photo do |f|
    f.date {  Faker::Date.backward(1) }
    f.file_name{Faker::Name.name}
    f.file_path{Faker::Name.name}
    f.checksum{Faker::Name.name}
  end
end
