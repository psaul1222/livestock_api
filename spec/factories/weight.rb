require 'faker'

FactoryBot.define do
  factory :weight do |f|
    f.date { Faker::Date.backward(365) }
    f.kilos {Faker::Number.decimal(3,3)}
  end
end
