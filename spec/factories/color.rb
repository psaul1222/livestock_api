require 'faker'

FactoryBot.define do
  factory :color do |f|
    f.color { Faker::Name.name }
  end
end
