require 'faker'

FactoryBot.define do

  factory :livestock do |f|
    f.name { Faker::Name.name }
    f.birthday { Faker::Date.backward(365) }
    association :tag, factory: :tag, strategy: :build
    association :gender, factory: :gender, strategy: :build
    association :primary_color, factory: :color, strategy: :build
    association :secondary_color, factory: :color, strategy: :build
    association :bought, factory: :transaction, strategy: :build
    association :sold, factory: :transaction, strategy: :build
    association :type, factory: :livestockType, strategy: :build
    association :primary_photo, factory: :photo, strategy: :build
    trait :father do
      association :father, factory: :livestock, strategy: :build
    end
    trait :mother do
      association :mother, factory: :livestock, strategy: :build
    end
  end
end
