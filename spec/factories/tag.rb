require 'faker'

FactoryBot.define do
  factory :tag do |f|
    f.number { Faker::Name.name }
    f.note { Faker::Name.name }
    association :color, factory: :color, strategy: :build
  end
end
