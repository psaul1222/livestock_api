require 'faker'

FactoryBot.define do
  factory :livestockType do |f|
    f.name { Faker::Name.name }
    trait :super_type do
      association :super_type, factory: :livestockType, strategy: :build
    end
  end
end
